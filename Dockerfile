FROM --platform=linux/x86_64 amazonlinux:2
SHELL ["/bin/bash", "-c"]
CMD ["/sbin/init"]
RUN yum -y update && yum clean all

# Apache2.4のインストール
RUN yum -y install httpd
# Apacheの自動起動設定
RUN systemctl enable httpd
# mysqlのインストール
RUN yum -y install mariadb
RUN amazon-linux-extras enable php7.4
RUN yum -y install php-cli php-pdo php-fpm php-json php-mysqlnd
RUN yum -y install php php-gd php-mbstring php-opcache php-xml php-common
RUN yum -y install wget
RUN wget https://wordpress.org/latest.tar.gz
RUN yum -y install tar
RUN tar zxf latest.tar.gz
RUN mv wordpress/* /var/www/html/
RUN cp /var/www/html/wp-config-sample.php /var/www/html/wp-config.php
RUN sed -i 's@database_name_here@wordpress@g' /var/www/html/wp-config.php
RUN sed -i 's@username_here@wordpress@g' /var/www/html/wp-config.php
RUN sed -i 's@password_here@wordpress@g' /var/www/html/wp-config.php
RUN sed -i 's@localhost@db@g' /var/www/html/wp-config.php
RUN chown -R apache:apache /var/www/html/*
